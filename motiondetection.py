#!/usr/bin/python

#Motion Detection Module for Colossus - The Raspberry Pi Aquarium Controller

import RPi.GPIO as GPIO
import time
from espeak import espeak
from subprocess import call
import os

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.IN)         #Read output from PIR motion sensor

while True:
       i=GPIO.input(11)
       if i==0:                 #When output from motion sensor is LOW
#             print "No intruders",i
             
             time.sleep(0.5)
       elif i==1:               #When output from motion sensor is HIGH
             print "Intruder detected",i
             os.system("/usr/bin/python /root/colossus-control/colossuscontrol.py floorlight")
             call(["espeak","-s140 -ven+18 -z","Motion Detected"])
             time.sleep (3)
             os.system("/usr/bin/python /root/colossus-control/colossuscontrol.py floorlight")
