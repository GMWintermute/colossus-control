#!/usr/bin/python

#
# HTML Output Module for Colossus Control Software 
#
# All HTML related output functions go here.
#


def output_html_header_refresh():
    print "<html><head>"
    print '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
    print '<link rel="stylesheet" href="http://192.168.1.107/fontawesome/css/font-awesome.min.css">'
    print '<meta http-equiv="refresh" content="62">'

    print "</head><body class='w3-black'>"

def output_html_footer():
    print "</body></html>"
    
def output_guage_header_refresh():
    print "<html><head>"
    print '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
    print '<link rel="stylesheet" href="http://192.168.1.107/fontawesome/css/font-awesome.min.css">'
    print '<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>'
    print '<meta http-equiv="refresh" content="62">'

    print "</head><body>"
